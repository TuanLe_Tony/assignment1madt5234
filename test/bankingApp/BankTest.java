package bankingApp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bankingApp.AccountDoesNotExistException;
import bankingApp.AccountExistsException;
import bankingApp.Bank;
import bankingApp.Currency;

public class BankTest {
	protected Currency CAD;
	protected Currency HKD;
	protected Bank RBC;
	protected Bank TD;
	protected Bank HSBC;
	
	
	@Before
	public void setUp() throws Exception {
		
		// setup some test currencies
		this.HKD = new Currency("HKD", 0.13);
		this.CAD = new Currency("CAD", 0.75);
		
		// setup test banks
		this.RBC = new Bank("Royal Bank of Canada", CAD);
		this.TD = new Bank("TD Bank", CAD);
		this.HSBC = new Bank("Hong Kong Shanghai Banking Corporation", HKD);
		
		// add sample customers to the banks
		
		
		// HINT:  uncomment these lines AFTER you test the openAccount() function
		// You can quickly uncomment / comment by highlighting the lines of code and pressing 
		// CTRL + / on your keyboard  (or CMD + / for Macs)
		
		this.RBC.openAccount("Marcos");
		this.RBC.openAccount("Albert");
		this.TD.openAccount("Jigesha");
		this.HSBC.openAccount("Pritesh");
	}

	@Test
	public void testGetName() {

		assertEquals("Royal Bank of Canada",RBC.getName());
		assertEquals(TD.getName(), "TD Bank");
		assertEquals(HSBC.getName(), "Hong Kong Shanghai Banking Corporation");
	}

	@Test
	public void testGetCurrency() {
		assertEquals(RBC.getCurrency(),CAD);
		assertEquals(TD.getCurrency(),CAD);
		assertEquals(HSBC.getCurrency(),HKD);
	}

	@Test
	public void testOpenAccount() throws AccountExistsException, AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		
		// See the example in class notes for testing exceptions;
		
		try
		{
			this.RBC.openAccount("Marcos");
	
			// But we did! So, somethings wrong with open account.
			
		}
		catch (AccountExistsException e)
		{
			System.out.println(e);
		}
		this.RBC.openAccount("Romil");
		
	}

	@Test
	public void testDeposit() throws AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		
		// See the example in class notes for testing exceptions.
//		RBC.deposit("Marcos", new Money(10000, CAD));
//		Double bal = RBC.getBalance("Marcos");
//		assertEquals(10000, bal,0.001);
	
		try
		{
			this.RBC.deposit("Tony", new Money(10000, CAD));
		
		}
		catch (Exception e)
		{

		}

					

	}

	@Test
	public void testWithdraw() throws AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		
		// See the example in class notes for testing exceptions.
		
		
		try {
			RBC.withdraw("Peter", new Money(100, CAD));
		}catch (AccountDoesNotExistException a) 
		{
			System.out.println(a + "failed withdraw");
			
		}
	}
	
	@Test
	public void testGetBalance() throws AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		
		// See the example in class notes for testing exceptions.
		
		try {
			this.RBC.getBalance("Marcos");
		}catch (AccountDoesNotExistException RBC) {
			System.out.println(RBC + "Checked failed");
			
		}
	}
	
	@Test
	public void testTransfer() throws AccountDoesNotExistException {
		// Note: You should test both types of transfers:
		// 1. Transfer from account to account
		// 2. Transfer between banks
		// See the Bank.java file for more details on Transfers
		try {
			RBC.transfer("Tony","100", new Money(100,CAD));;
		}catch (AccountDoesNotExistException RBC) {
			System.out.println(RBC + "Tranfer failed ");
			
		}
	}
	
}
