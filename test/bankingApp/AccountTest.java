package bankingApp;

import org.junit.Before;
import org.junit.Test;

import bankingApp.Account;
import bankingApp.AccountDoesNotExistException;
import bankingApp.Bank;
import bankingApp.Currency;
import bankingApp.Money;

import static org.junit.Assert.*;

import javax.swing.text.AbstractDocument.Content;

public class AccountTest {
	protected Currency CAD, HKD;
	protected Bank TD;
	protected Bank HSBC;
	protected Bank RBC;
	protected Account testAccount;
	
	@Before
	public void setUp() throws Exception {
		
		// setup test currencies
		CAD = new Currency("CAD", 0.75);
		RBC = new Bank("Royal Bank of Canada", CAD);
		
		// setup test accounts
		RBC.openAccount("Peter");
		testAccount = new Account("Albert", CAD);
		testAccount.deposit(new Money(100, CAD));

		// setup an initial deposit
		//RBC.deposit("Peter", new Money(500, CAD));
	}

	@Test
	public void testAddWithdraw() {
		
		
		testAccount.withdraw(new Money(100,CAD));
		
		assertFalse(testAccount.timedPaymentExists("rent"));
		
		testAccount.addTimedPayment("rent", 30, 0, new Money(10000, CAD), RBC, "Peter");
		assertTrue(testAccount.timedPaymentExists("rent"));
		
		testAccount.removeTimedPayment("rent");
		assertFalse(testAccount.timedPaymentExists("rent"));

		
		
		
		

	
	}
	
	@Test
	public void testGetBalance() {


		try
		{
			RBC.getBalance("aaaaa");
		}
		catch (AccountDoesNotExistException e)
		{
		}
		
	}
}
