package bankingApp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bankingApp.Currency;

public class CurrencyTest {
	
	/* Example currencies: 
	 * 	CAD = Canadian dollar
	 * 	EUR = Euros
	 * 	GBP = Great British Pounds
	 * 	HKD = Hong Kong Dollars
	 */
	public Currency CAD, EUR, GBP, HKD;
	
	@Before
	public void setUp() throws Exception {
		// Setup some test currencies to use in the below test cases
		CAD = new Currency("CAD", 0.75);
		EUR = new Currency("EUR", 1.14);
		HKD = new Currency("HKD", 0.13);
		//DONE and passes

	}

	@Test
	public void testGetName() {
		// Write the test case for testing the getName() function
		assertEquals(CAD.getName(),"CAD");
		assertEquals(EUR.getName(),"EUR");
		assertEquals(HKD.getName(),"HKD");
		
		//DONE and passes
		
	}
	
	@Test
	public void testGetRate() {
		// @TODO: Write the test case for testing the getRate() function
		assertEquals(CAD.getRate(),0.75,0.001);
		assertEquals(EUR.getRate(),1.14,0.001); 
		assertEquals(HKD.getRate(),0.13,0.001);
		
		//DONE and passes
	}
	
	@Test
	public void testSetRate() {
		// @TODO: Write the test case for testing the setRate() function
		
		// For this function, you should do:
		// 1. Assert that the oldRate is correct
		// 2. Change the rate
		// 3. Assert that the newRate is correct
		// You will end up with 2 assert() statements in this function.
		
		//----CODE CODE CODE HERE ----//
		
		assertEquals(CAD.getRate(), 0.75,0.001); // 1. old rate
		CAD.setRate(1.1); // 2. I set up the new rate 
		assertEquals(CAD.getRate(), 1.1,0.001); // 3. then check the new rate.
		
		//DONE and passes   -- 2 assert()
		
		
		
		
	}
	
	@Test
	public void testValueInUSD() {
		// @TODO: Write the test case for testing the valueInUSD() function
//		assertEquals(Currency.round(0.75, 0),CAD.valueInUSD(1),0);
		// DONE and passes --- the 0.75 CAD = 1 USD dollar.
		
		assertEquals(75, CAD.valueInUSD(100),0.001);
		
	}
	
	@Test
	public void testValueInThisCurrency() {
		// @TODO: Write the test case for testing the valueInThisCurrency() function
		// For this function, you should:
		// 1. Assert the value of the "other" currency
		// 2. Get the value in "this" currency
		// 3. Assert that the value in "this" currency is correct
		// You will end up with 2 assert() statements in this function.
		//---CODE CODE CODE HERE--- BLA BLA--- 
		
//		assertEquals(CAD.valueInThisCurrency(11, CAD),7.5,0);

		//test case 1 
//		assertEquals(Currency.round(7.5, 2),(CAD.valueInThisCurrency(10, CAD)),0); //CAD to US
//		assertEquals(Currency.round(27.78, 2),(CAD.valueInThisCurrency(7.5, CAD)),0); // US to BRL
		//test case 2
		assertEquals(17 ,CAD.valueInThisCurrency(100, HKD),0.001);
		assertEquals(Currency.round(17, 2),(CAD.valueInThisCurrency(100, HKD)),0.001); //CAD to US
		
	}

}
