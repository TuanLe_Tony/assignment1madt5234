package bankingApp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bankingApp.Currency;
import bankingApp.Money;

public class MoneyTest {
	protected Currency CAD, HKD, NOK, EUR;
	protected Money CAD100, EUR10, CAD200, EUR20, CAD0, EUR0, CADnegative100;
	
	@Before
	public void setUp() throws Exception {
		// setup sample currencies
		CAD = new Currency("CAD", 0.75);
		HKD = new Currency("HKD", 0.13);
		EUR = new Currency("EUR", 1.14);
		
		// setup sample money amounts
		CAD100 = new Money(100, CAD);
		EUR10 = new Money(10, EUR);
		CAD200 = new Money(200, CAD);
		
		EUR20 = new Money(20, EUR);
		CAD0 = new Money(0, CAD);
		EUR0 = new Money(0, EUR);
		CADnegative100 = new Money(-100, CAD);
	}

	@Test
	public void testGetAmount() {
		assertEquals(CAD100.getAmount(),100,0);
		assertEquals(EUR10.getAmount(),10,0);
		assertEquals(CAD200.getAmount(),200,0);

		assertEquals(EUR20.getAmount(),20,0);
		assertEquals(CAD0.getAmount(),0,0);
		assertEquals(EUR0.getAmount(),0,0);
		
		assertEquals(CADnegative100.getAmount(),-100,0);
		//should pick 1 in each currency to test avoiding Exhaust testing. :(
		
	}

	@Test
	public void testGetCurrency() {
		assertEquals(CAD100.getCurrency(), CAD);
		assertEquals(EUR10.getCurrency(), EUR);
		assertEquals(CAD200.getCurrency(), CAD);
		
		assertEquals(EUR20.getCurrency(), EUR);
		assertEquals(CAD0.getCurrency(), CAD);
		assertEquals(EUR0.getCurrency(), EUR);
		assertEquals(CADnegative100.getCurrency(), CAD);
		//should pick 1 in each currency to test avoiding Exhaust testing. :(
		
		
		
	}

	@Test
	public void testToString() {
		assertEquals(CAD100.toString(), "100.0");
		assertEquals(EUR10.toString(), "10.0");
		assertEquals(CAD200.toString(), "200.0");

		assertEquals(EUR20.toString(), "20.0");
		assertEquals(CAD0.toString(), "0.0");
		assertEquals(EUR0.toString(), "0.0");
		assertEquals(CADnegative100.toString(), "-100.0");
	}

	@Test
	public void testGetUniversalValue() {
		//passes test case
//		assertEquals(75,CAD100.getUniversalValue(),0);
		
		//failed
//		assertEquals(75,CAD200.getUniversalValue(),0);
		
		//another ways
		assertEquals(75, CAD100.getUniversalValue(),0.001);
	}

	@Test
	public void testEqualsMoney() {
		assertTrue(CAD100.equals(CADnegative100));
		assertTrue(CAD100.equals(EUR20));
		
		
	}

	@Test
	public void testAdd() {
		assertEquals(CAD100.getCurrency(), CAD200.add(EUR10).getCurrency());
		assertEquals(EUR20.getCurrency(), EUR10.add(CAD100).getCurrency());
		assertTrue(CAD100.equals(CAD200.add(EUR10)));

	}

	@Test
	public void testSubtract() {
		assertEquals(CAD200.getCurrency(), CAD100.subtract(EUR20).getCurrency());
		assertEquals(EUR20.getCurrency(), EUR10.subtract(CAD100).getCurrency());
		assertEquals(CADnegative100.getCurrency(), CAD100.subtract(EUR10).getCurrency());
	}

	@Test
	public void testIsZero() {
		
		assertEquals(CAD0.isZero(),true);
		assertEquals(CADnegative100.isZero(),false);
	}

	@Test
	public void testNegate() {
//		assertNull(CADnegative100.negate());
		assertEquals(CADnegative100.toString(), CAD100.negate().toString());
	}

	@Test
	public void testCompareTo() {
		assertEquals(CAD0.compareTo(CAD0),0);
		assertEquals(CAD0.compareTo(CAD100),0);
		assertEquals(CAD100.compareTo(CAD0),0);
	}
}
